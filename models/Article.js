var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');

var ArticleSchema = new mongoose.Schema({
  slug: {type: String, lowercase: true, unique: true},
  title: String,
  description: String,
  body: String,
  favorite: {type: Boolean, default: false},
  learningIndicator: {type: Number, default: 0},
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  tagList: [{ type: String }],
  actions: [{type: String}],
  timesWasAsked: {type: Number, default:0},
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
}, {timestamps: true});

ArticleSchema.plugin(uniqueValidator, {message: 'is already taken'});

ArticleSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

ArticleSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

ArticleSchema.methods.optimalTime = function (actions1) {
  let actions = actions1.filter((action) => {return action.timesAsked === this.timesWasAsked})
  if (actions.length === 0) {
    //this.actions.push(">", this.timesWasAsked, Date.now())
    return this.updatedAt.getTime() +  60000 * ((2.0) ** (this.timesWasAsked))
  } else {
    let more_count = [];
    let previous = 0
    actions.forEach((action) => {
      if (action.type === ">")
        more_count.push(previous + 1)
      else
        more_count.push(previous)
      previous = more_count[more_count.length-1]
    })
    let less_count = [];
    previous = 0;
    const reversed = actions.reverse();
    reversed.forEach((action) => {
      if (action.type === "<")
        less_count.push(previous + 1)
      else
        less_count.push(previous)
      previous = less_count[less_count.length-1]
    })
    //less_count = less_count.reverse();
    let mx = -1
    let index = -1
    for (let i = 0; i  < actions.length; i++) {
      let new_value = more_count[i] + less_count[i]
      if (mx === -1 || new_value > mx) {
        mx = new_value
        index = i
      }
    }
    if (index + 1 < actions.size) {
      return this.updatedAt.getTime() + (actions[index].time + actions[index + 1].time) / 2
    } else {
      return this.updatedAt.getTime() + actions[actions.length-1].time * 2
    }
  }
}


ArticleSchema.methods.toJSONFor = function(user){
  return {
    slug: this.slug,
    title: this.title,
    description: this.description,
    body: this.body,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    tagList: this.tagList,
    favorite: this.favorite,
    favoritesCount: this.favoritesCount,
    learningIndicator: this.learningIndicator,
    timesWasAsked: this.timesWasAsked,
    author: this.author.toProfileJSONFor(user)
  };
};

mongoose.model('Article', ArticleSchema);
