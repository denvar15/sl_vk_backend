var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');
var easyvk = require('easyvk')

easyvk({
  clientId: '7657902',
  clientSecret: 'AWHkCzYjpSsEeBKIS39M'
}).then(vk => {
  easyvk({
    token: "2ffd63412ffd63412ffd6341472f89baef22ffd2ffd63417057d0417679a00e2ef589cc"
  }).then(vk => {
    router.get('/user', function(req, res, next){
      User.findById(req.payload.id).then(function(user){
        if(!user){ return res.sendStatus(401); }

        return res.json({user: user.toAuthJSON()});
      }).catch(next);
    });

    router.put('/user', auth.required, function(req, res, next){
      User.findById(req.payload.id).then(function(user){
        if(!user){ return res.sendStatus(401); }

        // only update fields that were actually passed...
        if(typeof req.body.user.username !== 'undefined'){
          user.username = req.body.user.username;
        }
        if(typeof req.body.user.email !== 'undefined'){
          user.email = req.body.user.email;
        }
        if(typeof req.body.user.bio !== 'undefined'){
          user.bio = req.body.user.bio;
        }
        if(typeof req.body.user.image !== 'undefined'){
          user.image = req.body.user.image;
        }
        if(typeof req.body.user.password !== 'undefined'){
          user.setPassword(req.body.user.password);
        }

        return user.save().then(function(){
          return res.json({user: user.toAuthJSON()});
        });
      }).catch(next);
    });

    router.post('/users/login', function(req, res, next){
      if(!req.body.token){
        return res.status(422).json({errors: {token: "can't be blank"}});
      }

      vk.call("secure.checkToken", {
        token: req.body.token.access_token,
        ip: req.get('x-forwarded-for'),
      }).then((vkr) => {
        if (vkr.success === 1) {
          User.findOne({uid: req.body.user.id.toString()}).then(function(user){
            if(!user){
              var userNew = new User();

              userNew.username = req.body.user.id.toString();
              userNew.email = req.body.user.id.toString();
              userNew.uid = req.body.user.id.toString();
              userNew.name = req.body.user.first_name;

              userNew.save().then(function(){
                return res.json({user: userNew._id});
              }).catch(next);
            } else {
              return res.json({user: user._id});
            }
          }).catch(next);
        } else {
          return res.status(422).json({errors: {token: "is not valid"}});
        }
      })
    });

    router.post('/users/push', function(req, res, next){
      if(!req.body.token){
        return res.status(422).json({errors: {token: "can't be blank"}});
      }
      vk.call("secure.checkToken", {
        token: req.body.token.access_token,
        ip: req.get('x-forwarded-for'),
      }).then((vkr) => {
        if (vkr.success === 1) {
          window.setTimeout(() => vk.call("notifications.sendMessage", {
            message: 'Вы не повторяли ' + req.body.message + ' уже сутки!',
            user_ids: req.body.id,
          }).then((vkr) => {
            return vkr
          }), 100000000000)

          /*
          User.findOne({uid: req.body.user.id.toString()}).then(function(user){
            if(!user){
              var userNew = new User();

              userNew.username = req.body.user.first_name;
              userNew.uid = req.body.user.id.toString();

              userNew.save().then(function(){
                return res.json({user: userNew._id});
              }).catch(next);
            } else {
              return res.json({user: user._id});
            }
          }).catch(next);
           */
        } else {
          return res.status(422).json({errors: {token: "is not valid"}});
        }
      })
    });

    router.post('/users', function(req, res, next){
      var user = new User();

      user.username = req.body.user.username;
      user.email = req.body.user.email;
      user.setPassword(req.body.user.password);

      user.save().then(function(){
        return res.json({user: user.toAuthJSON()});
      }).catch(next);
    });
  })
})

module.exports = router;
